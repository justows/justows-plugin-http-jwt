//imports
const {catalog} = require("justo");
const babel = require("justo.plugin.babel");
const cli = require("justo.plugin.cli");
const eslint = require("justo.plugin.eslint");
const fs = require("justo.plugin.fs");
const npm = require("justo.plugin.npm");

//internal data
const PKG = require("./package.json").name;
const who = "justows";

//catalog
catalog.macro("lint", [
  {title: "Check Dogma code", task: cli, params: {cmd: "dogmac lint src test"}},
  {title: "Check JavaScript code", task: eslint, params: {src: "."}}
]).title("Lint source code");

catalog.macro("trans-dogma", [
  {title: "Transpile src", task: cli, params: {cmd: "dogmac js --min -o build/src src"}},
  {title: "Transpile test", task: cli, params: {cmd: "dogmac js --min -o build/test/unit test/unit"}}
]).title("Transpile from Dogma to JS");

catalog.macro("trans-js", [
  {task: babel, params: {src: "build", dst: `dist/${PKG}/`}}
]).title("Transpile from JS to JS");

catalog.macro("build", [
  {task: fs.rm, params: {path: "./build"}},
  {task: fs.rm, params: {path: "./dist"}},
  {task: catalog.get("trans-dogma")},
  {task: catalog.get("trans-js")},
  {task: fs.copy, params: {src: "package.json", dst: `dist/${PKG}/package.json`}},
  {task: fs.copy, params: {src: "README.md", dst: `dist/${PKG}/README.md`}},
  {task: fs.copy, params: {src: "./test/data/pub.pem", dst: `dist/${PKG}/test/data/pub.pem`}},
  {task: fs.copy, params: {src: "./test/data/pvt.pem", dst: `dist/${PKG}/test/data/pvt.pem`}}
]).title("Build package");

catalog.macro("make", [
  catalog.get("lint"),
  catalog.get("build")
]).title("Lint and build");

catalog.call("install", npm.install, {
  pkg: `./dist/${PKG}`,
  global: true,
  loglevel: "error"
}).title("Install package globally");

catalog.call("pub", npm.publish, {
  who,
  path: `dist/${PKG}`
}).title("Publish in NPM");

catalog.macro("test", `./dist/${PKG}/test/unit`).title("Unit testing");

catalog.macro("dflt", [
  catalog.get("lint"),
  catalog.get("build"),
  catalog.get("test")
]).title("Lint, build and test");

catalog.macro("keys", [
  {task: fs.rm, params: {path: "test/data/pvt.pem"}},
  {task: fs.rm, params: {path: "test/data/pub.pem"}},
  {task: cli, title: "Generate pvt key", params: {cmd: "openssl genpkey -algorithm RSA -out test/data/pvt.pem -pkeyopt rsa_keygen_bits:2048 2> /dev/null"}},
  {task: cli, title: "Derive pub key from pvt key", params: {cmd: "openssl rsa -pubout -in test/data/pvt.pem -out test/data/pub.pem 2> /dev/null"}}
]).title("Generate keys for signed tokens");
