# justows.plugin.http.jwt

[![NPM version](https://img.shields.io/npm/v/justows.plugin.http.jwt.svg)](https://npmjs.org/package/justows.plugin.http.jwt)
[![Total downloads](https://img.shields.io/npm/dt/justows.plugin.http.jwt.svg)](https://npmjs.org/package/justows.plugin.http.jwt)

Plugin for **JSON Web Token** support.

*Developed in [Dogma](http://dogmalang.com), compiled to JavaScript.*

*Engineered in Valencia, Spain, EU by Justo WebServices.*

## Description

This plugin provides a pre action for processing a **JSON Web Token** stored in a cookie or in the Authorization header.

## Use

The plugin returns a function which returns the plugin metadata:

```
function plugin(opts:object) : object
```

- `opts`:
  - `alg` (string, required). Algorithm to use: `HS256`, `HS384`, `HS512`, `RS256`, `RS384`, `RS512`.
  - `secret` (string, required when using `HS*`). Secret to use with `HS*`.
  - `key` (string, required when using `RS*`). Public key to use.
  - `cookie` (string). Cookie name where the JWT is stored.
  - `authorization` (bool). Is the token stored in the `Authorization` header? Default: `false`.
  - `field` (string). Field name where to save the decoded token in the `req` object. Default: `token`.
  - `iss` (string). `iss` claim to check.
  - `aud` (string). `aud` claim to check.
  - `exp` (bool). To check `exp` claim.

Example:

```
http: {
  plugins: [
    require("justows.plugin.http.jwt")({alg: "HS259", secret: "1234567890", cookie: "Token"})
  ]

  ...
}
```
