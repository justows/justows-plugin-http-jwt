"use strict";

var _dogmalangmin = require("dogmalangmin");

var _justo = require("justo");

const assert = _dogmalangmin.dogma.use(require("justo.assert"));const plugin = _dogmalangmin.dogma.use(require("../../../justows.plugin.http.jwt"));
module.exports = exports = (0, _justo.suite)(__filename, () => {
  {
    (0, _justo.test)("unknown alg", () => {
      {
        assert(() => {
          {
            plugin({ ["alg"]: "ES256", ["cookie"]: "good" });
          }
        }).raises("alg can only be HS* or RS*.");
      }
    });
  }
});