"use strict";

var _dogmalangmin = require("dogmalangmin");

var _justo = require("justo");

const assert = _dogmalangmin.dogma.use(require("justo.assert"));const plugin = _dogmalangmin.dogma.use(require("../../../justows.plugin.http.jwt"));
const COOKIE_TOKEN = { ["iss"]: "jwt plugin", ["sub"]: "me", ["type"]: "cookie" };
const AUTHORIZATION_TOKEN = { ["iss"]: "jwt plugin", ["sub"]: "me", ["type"]: "authorization" };
const ENCODED_COOKIE = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJqd3QgcGx1Z2luIiwic3ViIjoibWUiLCJ0eXBlIjoiY29va2llIn0.YjK3gdwIu74r4oqC-QoZW5tY228OLrbUqdk5DJtxjIo";
const ENCODED_AUTHORIZATION = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJqd3QgcGx1Z2luIiwic3ViIjoibWUiLCJ0eXBlIjoiYXV0aG9yaXphdGlvbiJ9.pO8K_AuU8l6hfJsAhSkle7jxgYdR8BXMc7Yhe_cc6J8";
module.exports = exports = (0, _justo.suite)(__filename, () => {
  {
    let req;(0, _justo.init)("*", () => {
      {
        req = _dogmalangmin.dogma.clone({ ["cookies"]: { ["get"]: name => {
              let val; /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("name", name, _dogmalangmin.text);{
                if (name == "good") {
                  val = ENCODED_COOKIE;
                } else {
                  val = _dogmalangmin.dogma.getSlice(ENCODED_COOKIE, 0, -2) + "J";
                }
              }return val;
            } }, ["headers"]: { ["get"]: name => {
              /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("name", name, _dogmalangmin.text);{
                return "Bearer " + ENCODED_AUTHORIZATION;
              }
            } } }, {}, {}, [], []);
      }
    }).title("Create HTTP request");(0, _justo.test)("secret expected", () => {
      {
        assert(() => {
          {
            plugin({ ["alg"]: "HS256", ["cookie"]: "good" });
          }
        }).raises("HS* requires secret.");
      }
    });(0, _justo.test)("Cookie", () => {
      {
        plugin({ ["alg"]: "HS256", ["secret"]: "mysecret", ["cookie"]: "good" }).init(req);assert(req).isMap();assert(req.token).is("Token").has(COOKIE_TOKEN);
      }
    });(0, _justo.suite)("Authorization", () => {
      {
        (0, _justo.test)("Bearer", () => {
          {
            plugin({ ["alg"]: "HS256", ["secret"]: "mysecret", ["authorization"]: true }).init(req);assert(req).isMap();assert(req.token).is("Token").has(AUTHORIZATION_TOKEN);
          }
        });(0, _justo.test)("Basic", () => {
          {
            plugin({ ["alg"]: "HS256", ["secret"]: "mysecret", ["authorization"]: true }).init(req = _dogmalangmin.dogma.clone(req, { "headers": { ["get"]: () => {
                  {
                    return "Basic YWxhZGRpbjpvcGVuc2VzYW1l";
                  }
                } } }, {}, [], []));assert(req).isMap();assert(req.token).is("Token").len(0);
          }
        });
      }
    });(0, _justo.test)("Both", () => {
      {
        plugin({ ["alg"]: "HS256", ["secret"]: "mysecret", ["cookie"]: "good", ["authorization"]: true }).init(req);assert(req).isMap();assert(req.token).is("Token").has(AUTHORIZATION_TOKEN);
      }
    });(0, _justo.test)("None", () => {
      {
        plugin({ ["alg"]: "HS256", ["secret"]: "mysecret" }).init(req);assert(req.token).is("Token").len(0);
      }
    });(0, _justo.test)("invalid token", () => {
      {
        plugin({ ["alg"]: "HS256", ["secret"]: "mysecret", ["cookie"]: "wrong" }).init(req);assert(req.token).is("Token").len(0);
      }
    });(0, _justo.test)("exp=false when no exp indicated in the token", () => {
      {
        plugin({ ["alg"]: "HS256", ["secret"]: "mysecret", ["cookie"]: "good", ["exp"]: false }).init(req);assert(req.token).is("Token").has(COOKIE_TOKEN);
      }
    });
  }
});