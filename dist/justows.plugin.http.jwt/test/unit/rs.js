"use strict";

var _dogmalangmin = require("dogmalangmin");

var _justo = require("justo");

const assert = _dogmalangmin.dogma.use(require("justo.assert"));const path = _dogmalangmin.dogma.use(require("path"));const fs = _dogmalangmin.dogma.use(require("fs"));const plugin = _dogmalangmin.dogma.use(require("../../../justows.plugin.http.jwt"));
const TOKEN = { ["iss"]: "jwt plugin", ["sub"]: "me", ["other"]: "salut!" };
const ENCODED = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJqd3QgcGx1Z2luIiwic3ViIjoibWUiLCJvdGhlciI6InNhbHV0ISJ9.vh9eNR2cayjn3oAUa9RVCkkQP9yjF_MMFDIUVKB2F6Qf728yHVbrx4iVv-CJBE-NExKYgqnZSkYGRDtPDf3Ij00Q1MBNYX_WdOSKfFhiQYgzj-dIL4uPY6o1-xSUsG6gKhOgr6ubH4aIqD7B7kAOOyodYBft9t4pfFx6GZD5PZJ1t8LXoBRnPrfyfkcQWclW_lEfMlL3m36ps8JMjM8jlMvt-LRwgoX-ZF8ykTvoStUVBTMvCf_Ak6A77T4wPlj-RZEHl-rLIkr5byWnqd49R-VvI5k5AQ2FPSgwxgyIjwuMm6IpLZrh--RFg3ylFJJWq5GK7FVrayz-EkuAgbGZWw";
module.exports = exports = (0, _justo.suite)(__filename, () => {
  {
    let req;(0, _justo.init)("*", () => {
      {
        req = _dogmalangmin.dogma.clone({ ["cookies"]: { ["get"]: name => {
              let val; /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("name", name, _dogmalangmin.text);{
                if (name == "good") {
                  val = ENCODED;
                } else {
                  val = _dogmalangmin.dogma.getSlice(ENCODED, 0, -2) + "J";
                }
              }return val;
            } }, ["headers"]: { ["get"]: name => {
              /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("name", name, _dogmalangmin.text);{
                return null;
              }
            } } }, {}, {}, [], []);
      }
    }).title("Create HTTP request");(0, _justo.test)("key expected", () => {
      {
        assert(() => {
          {
            plugin({ ["alg"]: "RS256", ["cookie"]: "good" });
          }
        }).raises("RS* requires key.");
      }
    });(0, _justo.test)("ok", () => {
      {
        plugin({ ["alg"]: "RS256", ["key"]: fs.readFileSync(path.join(__dirname, "../data/pub.pem")), ["cookie"]: "good" }).init(req);assert(req).isMap();assert(req.token).is("Token").has(TOKEN);
      }
    });(0, _justo.test)("failed", () => {
      {
        plugin({ ["alg"]: "RS256", ["key"]: fs.readFileSync(path.join(__dirname, "../data/pub.pem")), ["cookie"]: "wrong" }).init(req);assert(req).isMap();assert(req.token).is("Token").len(0);
      }
    });
  }
});