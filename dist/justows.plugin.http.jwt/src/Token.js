"use strict";

var _dogmalangmin = require("dogmalangmin");

const jwt = _dogmalangmin.dogma.use(require("jsonwebtoken"));
const $Token = class Token {
  constructor(claims) {
    /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("claims", claims, _dogmalangmin.map);{
      for (let [name, val] of Object.entries(claims)) {
        {
          _dogmalangmin.dogma.setItem("=", this, name, val);
        }
      }
    }
  }
};
const Token = new Proxy($Token, { apply(receiver, self, args) {
    return new $Token(...args);
  } });module.exports = exports = Token;
Token.from = function (req, opts) {
  let tok; /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("req", req, null); /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("opts", opts, _dogmalangmin.map);try {
    let enc;if (opts.authorization) {
      if ((enc = req.headers.get("Authorization")) && _dogmalangmin.dogma.like(enc, "^Bearer ")) {
        enc = enc.replace(RegExp("^Bearer +"), "");
      }
    }if (!enc && opts.cookie) {
      enc = req.cookies.get(opts.cookie);
    }tok = Token(jwt.verify(enc, _dogmalangmin.dogma.like(opts.alg, "^HS*") ? opts.secret : opts.key, { ["algorithm"]: opts.alg, ["issuer"]: opts.iss, ["audience"]: opts.aud, ["ignoreExpiration"]: _dogmalangmin.dogma.includes(opts, "exp") ? opts.exp == false : null }));
  } catch (e) {
    return Token({});
  }return tok;
};