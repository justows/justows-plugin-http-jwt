"use strict";

var _dogmalangmin = require("dogmalangmin");

const Token = _dogmalangmin.dogma.use(require("./Token"));
function plugin(opts) {
  /* istanbul ignore next */_dogmalangmin.dogma.paramExpectedToHave("opts", opts, { alg: { type: _dogmalangmin.text, mandatory: true }, secret: { type: _dogmalangmin.text, mandatory: false }, cookie: { type: _dogmalangmin.text, mandatory: false }, authorization: { type: _dogmalangmin.bool, mandatory: false }, field: { type: _dogmalangmin.text, mandatory: false }, iss: { type: _dogmalangmin.text, mandatory: false }, aud: { type: _dogmalangmin.text, mandatory: false }, exp: { type: _dogmalangmin.bool, mandatory: false } }, true);{
    if (_dogmalangmin.dogma.like(opts.alg, "^HS*")) {
      if (!opts.secret) {
        _dogmalangmin.dogma.raise("HS* requires secret.");
      }
    } else if (_dogmalangmin.dogma.like(opts.alg, "^RS*")) {
      if (!opts.key) {
        _dogmalangmin.dogma.raise("RS* requires key.");
      }
    } else {
      _dogmalangmin.dogma.raise("alg can only be HS* or RS*.");
    }return { ["init"]: req => {
        /* istanbul ignore next */_dogmalangmin.dogma.paramExpected("req", req, null);{
          _dogmalangmin.dogma.setItem("=", req, opts.field || "token", Token.from(req, opts));
        }
      } };
  }
}module.exports = exports = plugin;